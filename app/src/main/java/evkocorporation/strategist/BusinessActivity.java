package evkocorporation.strategist;

import android.app.NotificationManager;
import android.content.Context;
import android.content.Intent;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import java.util.ArrayList;

import evkocorporation.strategist.Entities.Message;

public class BusinessActivity extends AppCompatActivity
        implements NavigationView.OnNavigationItemSelectedListener{
    private RecyclerView recyclerView;
    private int itemPosition;
    private BusinessCardAdapter adapter;
    private NotificationManager notificationManager;
    private SearchView searchView;
    ArrayList<Message> messageList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_business);
        //Запуск панели
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        //Кнопка бургер
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar,
                R.string.open_drawer,
                R.string.close_drawer);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        //Панель навигации
        NavigationView navigation =(NavigationView) findViewById(R.id.nav_view);
        navigation.setNavigationItemSelectedListener(this);
        //Текущая активность выделенна
        navigation.setCheckedItem(R.id.nav_business);
        notificationManager = (NotificationManager) this.getSystemService(Context.NOTIFICATION_SERVICE);
        //Инициализация списка
        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        //Инициализация поиска
        searchView = (SearchView) findViewById(R.id.search_view);
        searchMessage();
        //Заполнение списка
        loadDatabase();
        //Регистрация контекстного меню
        registerForContextMenu(recyclerView);
    }

    //Поиск
    private void searchMessage(){
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String word) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String word) {
                if (!word.equals("")) {
                    ArrayList<Message> searchList = new ArrayList<>();
                    for (Message message : messageList) {
                        if ((message.getMessageTitle().contains(word)) ||
                                (message.getMessageText().contains(word))) {
                            searchList.add(message);
                        }
                    }
                    BusinessCardAdapter searchAdapter =
                            new BusinessCardAdapter(BusinessActivity.this, searchList);
                    recyclerView.setAdapter(searchAdapter);
                }else{
                    recyclerView.setAdapter(adapter);
                }
                return false;
            }
        });
    }

    //Заполнение RecyclerView из БД
    public void loadDatabase() {
        String sqlString = DatabaseHelper.createMessageSQLString();
        messageList = DatabaseHelper.getMessage(this, sqlString);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        adapter = new BusinessCardAdapter(this, messageList);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setHasFixedSize(true);
        recyclerView.setAdapter(adapter);
        //вывод контекстного меню
        adapter.setListener(new CardAdapter.Listener() {
            @Override
            public void onLongClick(View v ,int position) {
                itemPosition = position;
                v.showContextMenu();
            }
        });
    }

    //Получение данных для сохдания контекстного меню
    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);
        MenuInflater inflater = this.getMenuInflater();
        inflater.inflate(R.menu.business_context_menu, menu);
    }

    //Действия при нажатии на контекстное меню
    @Override
    public boolean onContextItemSelected(MenuItem item) {
        Message message = messageList.get(itemPosition);
        int id = item.getItemId();
        //Кнопка скрыть
        if (id == R.id.hide) {
            notificationManager.cancel(message.getNotificationId());
        }
        //Кнопка активировать
        if (id == R.id.upg) {
            Message.showMessage(this, notificationManager,
                    message.getMessageTitle(),
                    message.getMessageText(),
                    message.getNotificationId());
        }
        //Кнопка удалить
        if (id == R.id.del) {
            DatabaseHelper.delete(this, DatabaseHelper.DB_BUSINESS, message.getId());
            notificationManager.cancel(message.getNotificationId());
            messageList.remove(itemPosition);
            recyclerView.getAdapter().notifyDataSetChanged();
            Toast.makeText(this, "Запись успешно удалена!", Toast.LENGTH_SHORT).show();
        }
        //кнопка изменить
        if (id == R.id.chan) {
            Bundle bundle = new Bundle();
            bundle.putString("text", message.getMessageText());
            bundle.putString("title", message.getMessageTitle());
            bundle.putString("id", Integer.toString(message.getId()));
            bundle.putString("notify", Integer.toString(message.getNotificationId()));
            Intent intent = new Intent(this, CreateBusinessActivity.class);
            intent.putExtras(bundle);
            startActivity(intent);
        }
        return true;
    }

    //Реакция на щелчки по панели
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        int id = item.getItemId();
        Intent intent = null;

        switch (id) {
            case R.id.nav_main:
                intent = new Intent(this, MainActivity.class);
                break;
            case R.id.nav_coming:
                intent = new Intent(this, ComingActivity.class);
                break;
            case R.id.nav_costs:
                intent = new Intent(this, CostsActivity.class);
                break;
            case R.id.nav_debits:
                intent = new Intent(this, DebitActivity.class);
                break;
            case R.id.nav_i_debits:
                intent = new Intent(this, IDebitActivity.class);
                break;
            default:
                intent = new Intent(this, BusinessActivity.class);
        }

        startActivity(intent);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    //закрытие панели при нажатии кнопки назад
    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            Intent intent = new Intent(this, MainActivity.class);
            startActivity(intent);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    //Открытие активности по щелчку на +
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.create_new:
                Intent intent = new Intent(this, CreateBusinessActivity.class);
                startActivity(intent);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }
}
