package evkocorporation.strategist;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.Collections;
import java.util.List;

import evkocorporation.strategist.Entities.Message;

public class BusinessCardAdapter extends RecyclerView.Adapter<BusinessCardAdapter.ViewHolder> {
    private Activity activity;
    List<Message> message = Collections.emptyList();
    private CardAdapter.Listener listener;

    public interface Listener {
        void onLongClick(View v, int position);
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView textView;
        public TextView TitleView;

        //Конструктор
        public ViewHolder(View v) {
            super(v);
            textView = (TextView) v.findViewById(R.id.card_text);
            TitleView = (TextView) v.findViewById(R.id.card_title);
        }
    }
    //Конструктор
    public BusinessCardAdapter(Activity activity, List<Message> message){
        this.activity = activity;
        this.message = message;
    }
    //Получение кол-ва карточек
    @Override
    public  int getItemCount(){
        return message.size();
    }

    public void setListener(CardAdapter.Listener listener) {
        this.listener = listener;
    }

    //Заполнение карточек
    @Override
    public void onBindViewHolder(ViewHolder viewHolder, final int position) {
        viewHolder.textView.setText(message.get(position).getMessageText());
        viewHolder.TitleView.setText(message.get(position).getMessageTitle());
        viewHolder.itemView.setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View view) {
                if (listener != null) {
                    listener.onLongClick(view, position);
                }
                return true;
            }
        });
    }
    //Связывание с макетом карточки
    @Override
    public BusinessCardAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.card_message, parent, false);
        return new BusinessCardAdapter.ViewHolder(v);
    }
}
