package evkocorporation.strategist;

import android.app.Activity;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.Collections;
import java.util.List;

import evkocorporation.strategist.Entities.Pay;


public class CardAdapter extends RecyclerView.Adapter<CardAdapter.ViewHolder> {
    private Activity activity;
    List<Pay> pays = Collections.emptyList();
    private Listener listener;

    public interface Listener {
        void onLongClick(View v, int position);
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {
        public TextView whoTextView;
        public TextView howManyTextView;
        public TextView dateTextView;
        public TextView categoryTextView;

//Конструктор
        public ViewHolder(View v) {
            super(v);
            whoTextView = (TextView) v.findViewById(R.id.Who_pay);
            howManyTextView = (TextView) v.findViewById(R.id.How_many_pay);
            dateTextView = (TextView)v.findViewById(R.id.date_pay);
            categoryTextView = (TextView)v.findViewById(R.id.category);
        }
    }
    //Конструктор
    public CardAdapter(Activity activity, List<Pay> pays){
        this.activity = activity;
        this.pays = pays;
    }
     //Получение кол-ва карточек
        @Override
        public  int getItemCount(){
            return pays.size();
        }

        public void setListener(Listener listener) {
        this.listener = listener;
        }
        //Заполнение карточек
        @Override
        public void onBindViewHolder(ViewHolder viewHolder,final int position) {
            viewHolder.whoTextView.setText(pays.get(position).getTitle());
            viewHolder.howManyTextView.setText(Integer.toString(pays.get(position).getPrice()));
            viewHolder.dateTextView.setText(receivingDate(pays.get(position).getDatePay()));
            viewHolder.categoryTextView.setText(pays.get(position).getCategory());
            viewHolder.itemView.setOnLongClickListener(new View.OnLongClickListener() {
                @Override
                public boolean onLongClick(View view) {
                    if (listener != null) {
                        listener.onLongClick(view, position);
                    }
                    return true;
                }
            });
        }
        //Связывание с макетом карточки
        @Override
        public CardAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
            View v = LayoutInflater.from(parent.getContext())
                    .inflate(R.layout.card_many, parent, false);
            return new ViewHolder(v);
        }

    private static String receivingNumber(int number) {
        String strNumber = "";
        if (number < 10) {
            strNumber = "0" + Integer.toString(number);
        } else {
            strNumber = Integer.toString(number);
        }
        return strNumber;
    }

    private static String receivingDate(String date) {
        String[] today = date.split("/");
        int day = Integer.parseInt(today[2]);
        int month = Integer.parseInt(today[1]);
        int year = Integer.parseInt(today[0]);
        String newDate = (receivingNumber(day) + "." + receivingNumber(month) + "." +receivingNumber(year));
        return newDate;
    }
}
