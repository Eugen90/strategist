package evkocorporation.strategist;

import android.app.NotificationManager;
import android.content.Intent;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import evkocorporation.strategist.Entities.Message;

public class CreateBusinessActivity extends AppCompatActivity {
    private String id;
    private String notifyId;
    public int NOTIFICATION_ID = 54;
    private EditText editTitle;
    private EditText editText;
    private NotificationManager notificationManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_business);
        //Запуск панели
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        //Кнопка назад
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        //Инициализация полей
        editText = (EditText) findViewById(R.id.text_enter);
        editTitle = (EditText) findViewById(R.id.title_enter);
        //Заполнение данными во время изменений
        editText.setText(getIntent().getStringExtra("text"));
        editTitle.setText(getIntent().getStringExtra("title"));
        id = getIntent().getStringExtra("id");
        notifyId = getIntent().getStringExtra("notify");

        notificationManager = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
        NOTIFICATION_ID = DatabaseHelper.getNotificationId(this) + 1;
    }

    //Создание меню с кнопкой
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_yes, menu);
        return super.onCreateOptionsMenu(menu);
    }

    //Создание записи по шелчку на галочке
    public void onClickDone(View view) {
        String text = editText.getText().toString();
        String title = editTitle.getText().toString();
        String messageText = "Запись успешно дабавлена!";

            //изменение
            if (id != null) {
                DatabaseHelper.updateMessage(this, title, text, id, notifyId);
                messageText = "Запись успешно изменена!";
                NOTIFICATION_ID = Integer.parseInt(notifyId);
            } else {
                //Добавление записи в БД
                DatabaseHelper.insertPay(this, DatabaseHelper.DB_BUSINESS, null,
                        -1, null, null, -1,
                        text, title, NOTIFICATION_ID, null);
            }
        Message.showMessage(this, notificationManager, title, text, NOTIFICATION_ID);

        //Сообщаем инфу пользователю
        Toast.makeText(this, messageText, Toast.LENGTH_SHORT).show();

        Intent intent = new Intent(this, BusinessActivity.class);
        startActivity(intent);
    }
}
