package evkocorporation.strategist;

import android.content.Intent;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import evkocorporation.strategist.Entities.Date;


public class CreateComingActivity extends AppCompatActivity {
    private String id;
    private EditText nameText;
    private EditText priseText;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_coming);
        //Запуск панели
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        //Кнопка назад
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        //Инициализация полей
        nameText = (EditText) findViewById(R.id.name_enter);
        priseText = (EditText) findViewById(R.id.sum_enter);
        //Заполнение данными во время изменений
            nameText.setText(getIntent().getStringExtra("title"));
            priseText.setText(getIntent().getStringExtra("price"));
            id = getIntent().getStringExtra("id");
    }
    //Создание меню с кнопкой
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_yes, menu);
        return super.onCreateOptionsMenu(menu);
    }

    //Создание записи по шелчку на галочке
    public void onClickDone(View view) {
        String name = nameText.getText().toString();
        int prise = Integer.parseInt(priseText.getText().toString());
        //Получение текущей даты
        Date todayDate = new Date();
        String date = todayDate.getDate();
        String messageText = "Запись успешно дабавлена!";

            //Изменяется запись!
            if (id != null) {
                DatabaseHelper.updatePay(this, DatabaseHelper.DB_COMING, name, prise,
                        id, -1, null);
                messageText = "Запись успешно изменена!";
            } else {
                //Добавление записи в БД
                DatabaseHelper.insertPay(this, DatabaseHelper.DB_COMING, name, prise, date,
                        null, -1, null, null, -1,
                        null);
            }

        //Сообщаем инфу пользователю
        Toast toast = Toast.makeText(this, messageText, Toast.LENGTH_SHORT);
        toast.show();

        Intent intent = new Intent(this, ComingActivity.class);
        startActivity(intent);
    }
}
