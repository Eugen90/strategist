package evkocorporation.strategist;

import android.content.DialogInterface;
import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.ArrayList;

import evkocorporation.strategist.Entities.Category;
import evkocorporation.strategist.Entities.Date;

public class CreateCostsActivity extends AppCompatActivity {
    private Spinner spinner;
    private EditText nameText;
    private EditText priseText;
    private int categoryId = 1;
    private String id;
    private ArrayList<Category> categoryList = new ArrayList<>();
    private SharedPreferences preferences;
    private String textCategory;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_costs);
        //Запуск панели
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        //Кнопка назад
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        //Инициализация полей
        nameText = (EditText) findViewById(R.id.name_enter);
        priseText = (EditText) findViewById(R.id.sum_enter);
        spinner = (Spinner) findViewById(R.id.spinner);

        preferences = getSharedPreferences("properties", MODE_PRIVATE);
        textCategory = preferences.getString("category", "Все категории");

        follSpinner();

        //Заполнение данными во время изменений
        nameText.setText(getIntent().getStringExtra("title"));
        priseText.setText(getIntent().getStringExtra("price"));
        String category = getIntent().getStringExtra("categoryId");
        id = getIntent().getStringExtra("id");
        if (category != null) {
            categoryId = Integer.parseInt(category) - 1;
            spinner.setSelection(categoryId);
        }

        spinnerSelect();
    }

    //Выбор категории
    private void spinnerSelect(){
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
                categoryId = categoryList.get(position).getId();
            }
            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
            }
        });

    }

    //Создание меню с кнопкой
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_yes, menu);
        return super.onCreateOptionsMenu(menu);
    }

    //Добавление категории
    public void addCategory(View view) {
        //Получаем вид с файла new_categery.xml, который применим для диалогового окна:
        LayoutInflater li = LayoutInflater.from(this);
        View promptsView = li.inflate(R.layout.new_categery, null);
        //Создаем AlertDialog
        AlertDialog.Builder mDialogBuilder = new AlertDialog.Builder(this);
        //Настраиваем new_categery.xml для нашего AlertDialog:
        mDialogBuilder.setView(promptsView);
        //Настраиваем отображение поля для ввода текста в открытом диалоге:
        final EditText userInput = (EditText) promptsView.findViewById(R.id.cat_text);
        //Настраиваем сообщение в диалоговом окне:
        mDialogBuilder
                .setCancelable(false)
                .setPositiveButton("OK",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog,int id) {
                                //Записываем в БД
                                insertCategory(userInput.getText().toString());
                                Intent intent = new Intent(getBaseContext(), CreateCostsActivity.class);
                                startActivity(intent);
                            }
                        })
                .setNegativeButton("Отмена",
                        new DialogInterface.OnClickListener() {
                            public void onClick(DialogInterface dialog, int id) {
                                dialog.cancel();
                            }
                        });
        //Создаем AlertDialog:
        AlertDialog alertDialog = mDialogBuilder.create();
        //и отображаем его:
        alertDialog.show();

    }

    //Сохранение категории
    private void insertCategory(String category){
        String messageText = "Запись успешно дабавленна!";
            //Добавление записи в БД
            DatabaseHelper.insertPay(this, DatabaseHelper.DB_COSTS_CATEGORY,null, -1,
                    null, null, -1, null, null, -1,
                    category);
        //Сообщаем инфу пользователю
        Toast.makeText(this, messageText, Toast.LENGTH_SHORT).show();
    }

    //Заполнение списка
    private void follSpinner() {
        categoryList = DatabaseHelper.getCategory(this);
        ArrayAdapter<Category> adapter = new ArrayAdapter<>(this,
                android.R.layout.simple_spinner_item, categoryList);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
        spinner.setSelection(getCategoryByName(textCategory));
    }

//Создание записи по шелчку на галочке
    public void onClickDone(View view) {
        String name = nameText.getText().toString();
        int prise = Integer.parseInt(priseText.getText().toString());
        //Получение текущей даты
        Date todayDate = new Date();
        String date = todayDate.getDate();
        //Получение Id категории
        String messageText = "Запись успешно дабавлена!";

            //Изменяется запись!
            if (id != null) {
                DatabaseHelper.updatePay(this, DatabaseHelper.DB_COSTS, name, prise,
                        id, categoryId, null);
                messageText = "Запись успешно изменена!";
            } else {
                //Добавление записи в БД
                DatabaseHelper.insertPay(this, DatabaseHelper.DB_COSTS, name, prise, date,null,
                        categoryId, null, null, -1, null);
            }
        //Сообщаем инфу пользователю
        Toast.makeText(this, messageText, Toast.LENGTH_SHORT).show();

        Intent intent = new Intent(this, CostsActivity.class);
        startActivity(intent);
    }

    //Удаление категории
    public void deleteCategory(View view) {
        Intent intent = new Intent(this, DeleteCategoryActivity.class);
        startActivity(intent);
    }

    private int getCategoryByName(String name){
        for(int i = 0; i < categoryList.size(); i++){
            if (categoryList.get(i).getCategory().equals(name)) return i;
        }
        return 0;
    }
}
