package evkocorporation.strategist;

import android.content.ContentValues;
import android.content.Intent;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.Menu;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import evkocorporation.strategist.Entities.Date;

public class CreateIDebitActivity extends AppCompatActivity {
    private EditText nameText;
    private EditText priseText;
    private String id;
    private String variable = "0";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_create_idebit);
        //Запуск панели
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        //Кнопка назад
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        //Инициализация полей
        nameText = (EditText) findViewById(R.id.name_enter);
        priseText = (EditText) findViewById(R.id.sum_enter);
        //Заполнение данными во время изменений
        nameText.setText(getIntent().getStringExtra("title"));
        priseText.setText(getIntent().getStringExtra("price"));
        id = getIntent().getStringExtra("id");
    }

    //Создание меню
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu_yes, menu);
        return super.onCreateOptionsMenu(menu);
    }

    //Создание записи по шелчку на галочке
    public void onClickDone(View view) {
        String name = nameText.getText().toString();
        int prise = Integer.parseInt(priseText.getText().toString());
        //Получение текущей даты
        Date todayDate = new Date();
        String date = todayDate.getDate();
        String messageText = "Запись успешно дабавлена!";

        //Изменяется запись!
        if (id != null) {
            DatabaseHelper.updatePay(this, DatabaseHelper.DB_DEBIT, name, prise,
                    id, -1, variable);
            messageText = "Запись успешно изменена!";
        } else {
            //Добавление записи в БД
            DatabaseHelper.insertPay(this, DatabaseHelper.DB_DEBIT, name, prise, date, variable,
                    -1, null, null, -1, null);
        }
        //Сообщаем инфу пользователю
        Toast.makeText(this, messageText, Toast.LENGTH_SHORT).show();

        Intent intent = new Intent(this, IDebitActivity.class);
        startActivity(intent);
    }
}
