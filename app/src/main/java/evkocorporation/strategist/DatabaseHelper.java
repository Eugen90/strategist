package evkocorporation.strategist;


import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.util.ArrayList;

import evkocorporation.strategist.Entities.Category;
import evkocorporation.strategist.Entities.Message;
import evkocorporation.strategist.Entities.Pay;

public class DatabaseHelper extends SQLiteOpenHelper {

    public static final String DB_NAME = "Strategist";
    public  static final String DB_COMING = "COMING";
    public static final String DB_COSTS = "COSTS";
    public static final String DB_COSTS_CATEGORY = "CATEGORY";
    public static final String DB_DEBIT = "DEBIT";
    public static final String DB_BUSINESS = "BUSINESS";
    private static final int DB_VERSION = 2;

    public DatabaseHelper(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }
//Создание БД
    @Override
    public void onCreate(SQLiteDatabase db) {
        db.execSQL("CREATE TABLE " + DB_COMING + " (_id INTEGER PRIMARY KEY AUTOINCREMENT," +
                "NAME TEXT," +
                "PRISE INTEGER," +
                "DATE TEXT);");

        db.execSQL("CREATE TABLE " + DB_COSTS_CATEGORY + "(_id INTEGER PRIMARY KEY AUTOINCREMENT," +
                "CATEGORY TEXT);");

        ContentValues personValue = new ContentValues();
        personValue.put("CATEGORY", "Все категории");
        db.insert(DB_COSTS_CATEGORY, null, personValue);

        db.execSQL("CREATE TABLE " + DB_COSTS + " (_id INTEGER PRIMARY KEY AUTOINCREMENT," +
                "NAME TEXT," +
                "PRISE INTEGER," +
                "DATE TEXT," +
                "CATEGORY_ID INTEGER NOT NULL," +
                "FOREIGN KEY (CATEGORY_ID)" +
                "REFERENCES " + DB_COSTS_CATEGORY + "(_id));");

        db.execSQL("CREATE TABLE " + DB_DEBIT + " (_id INTEGER PRIMARY KEY AUTOINCREMENT," +
                "NAME TEXT," +
                "PRISE INTEGER," +
                "DATE TEXT," +
                "VARIABLE TEXT);");

        db.execSQL("CREATE TABLE " + DB_BUSINESS + " (_id INTEGER PRIMARY KEY AUTOINCREMENT," +
                "TEXT TEXT," +
                "TITLE TEXT," +
                "NOT_ID INTEGER);");
    }


    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
    }

    //Запись в БД
    public static void insertPay(Context context, String tableName, String name, int prise,
                                 String date, String variable, int categoryId, String text,
                                 String title, int notificationId, String category) {
        SQLiteOpenHelper openHelper = new DatabaseHelper(context);
        SQLiteDatabase db = openHelper.getReadableDatabase();
        ContentValues personValue = new ContentValues();
        if (name != null) personValue.put("NAME", name);
        if (prise >= 0) personValue.put("PRISE",prise);
        if (date != null) personValue.put("DATE", date);
        if (variable != null) personValue.put("VARIABLE", variable);
        if (categoryId >= 0) personValue.put("CATEGORY_ID", categoryId);
        if (text != null) personValue.put("TEXT", text);
        if (title != null) personValue.put("TITLE", title);
        if (notificationId >= 0) personValue.put("NOT_ID", notificationId);
        if (category != null) personValue.put("CATEGORY", category);
        db.insert(tableName, null, personValue);
    }

    public static ArrayList<Pay> getPay(Context context, String activity, String sqlString){
            ArrayList<Pay> payList = new ArrayList<>();
        SQLiteOpenHelper openHelper = new DatabaseHelper(context);
        SQLiteDatabase db = openHelper.getReadableDatabase();
            Cursor cursor = db.rawQuery(sqlString, null);
            if (cursor.moveToFirst()) {
                do {
                    Pay pay = new Pay();
                    pay.setId(cursor.getInt(0));
                    pay.setTitle(cursor.getString(1));
                    pay.setPrice(cursor.getInt(2));
                    pay.setDatePay(cursor.getString(3));
                    if (activity.equals("costs")) pay.setCategory(receiveCategory(context, cursor.getInt(4)));
                    payList.add(pay);
                } while (cursor.moveToNext());
            }
            cursor.close();
            db.close();
            return payList;
    }

    public static ArrayList<Message> getMessage(Context context, String sqlString){
        ArrayList<Message> messageList = new ArrayList<>();
        SQLiteOpenHelper openHelper = new DatabaseHelper(context);
        SQLiteDatabase db = openHelper.getReadableDatabase();
        Cursor cursor = db.rawQuery(sqlString, null);
        if (cursor.moveToFirst()) {
            do {
                Message message = new Message();
                message.setId(cursor.getInt(0));
                message.setMessageText(cursor.getString(1));
                message.setMessageTitle(cursor.getString(2));
                message.setNotificationId(cursor.getInt(3));
                messageList.add(message);
            } while (cursor.moveToNext());
        }
        cursor.close();
        db.close();
        return messageList;
    }

    public static void updateMessage(Context context, String title, String text,
                                 String id, String notyId){
        SQLiteOpenHelper openHelper = new DatabaseHelper(context);
        SQLiteDatabase db = openHelper.getReadableDatabase();
        ContentValues value = new ContentValues();
        value.put("TITLE", title);
        value.put("TEXT", text);
        value.put("NOT_ID", Integer.parseInt(notyId));
        db.update(DB_BUSINESS,
                value, "_id = ?", new String[] {id});
        db.close();
    }

    public static ArrayList<Category> getCategory(Context context){
        ArrayList<Category> categoryList = new ArrayList<>();
        SQLiteOpenHelper openHelper = new DatabaseHelper(context);
        SQLiteDatabase db = openHelper.getReadableDatabase();
        Cursor cursor = db.query(DB_COSTS_CATEGORY,
                new String[]{"CATEGORY", "_id"},
                null,null, null, null, null);
        if (cursor.moveToFirst()) {
            do {
                Category category = new Category();
                category.setCategory(cursor.getString(0));
                category.setId(cursor.getInt(1));
                categoryList.add(category);
            } while (cursor.moveToNext());
        }
        return categoryList;
    }

    public static void updatePay(Context context, String tableName, String title, int price,
                                 String id, int categoryId, String variable){
        SQLiteOpenHelper openHelper = new DatabaseHelper(context);
        SQLiteDatabase db = openHelper.getReadableDatabase();
        ContentValues value = new ContentValues();
        if (title != null) value.put("NAME", title);
        if (price >= 0) value.put("PRISE", price);
        if (categoryId >= 0) value.put("CATEGORY_ID", categoryId);
        if (variable != null) value.put("VARIABLE", variable);
        db.update(tableName,
                value, "_id = ?", new String[] {id});
        db.close();
    }

    public static int getSum(Context context, String sqlString){
        int sum = 0;
        SQLiteOpenHelper openHelper = new DatabaseHelper(context);
        SQLiteDatabase db = openHelper.getReadableDatabase();
        Cursor cursor = db.rawQuery(sqlString, null);
        if (cursor.moveToFirst()) {
            sum = cursor.getInt(0);
        }
        cursor.close();
        db.close();
        return sum;
    }

    private static String receiveCategory(Context context, int id) {
        String category = "";
        SQLiteOpenHelper openHelper = new DatabaseHelper(context);
        SQLiteDatabase db = openHelper.getReadableDatabase();
        Cursor categoryCursor = db.query(DatabaseHelper.DB_COSTS_CATEGORY,
                    new String[]{"CATEGORY"},
                    "_id = ?",
                    new String[] {Integer.toString(id)}, null, null, null);
            if (categoryCursor.moveToFirst()) {
                category = categoryCursor.getString(0);
            }
            categoryCursor.close();
        db.close();
        return category;
    }

    public static int receiveIdCategory(Context context, String category) {
        int id = 0;
        SQLiteOpenHelper openHelper = new DatabaseHelper(context);
        SQLiteDatabase db = openHelper.getReadableDatabase();
        Cursor categoryCursor = db.query(DatabaseHelper.DB_COSTS_CATEGORY,
                    new String[]{"_id"},
                    "CATEGORY = ?",
                    new String[] {category}, null, null, null);
            if (categoryCursor.moveToFirst()) {
                id = categoryCursor.getInt(0);
            }
            categoryCursor.close();
        db.close();
        return id;
    }

    public static String createSqlString(Context context, String tableName, String decision,
                                         String firstDate, String secondDate, String variable,
                                         String category, String name, int prise){
        String command = "*";
        if (decision != null) command = "SUM(PRISE)";

        String sqlString = "SELECT " + command + " FROM " + tableName + " WHERE ";
        if (variable != null) sqlString += "VARIABLE = '" + variable + "' AND ";
        if (category != null) sqlString += "CATEGORY_ID = " + receiveIdCategory(context, category) + " AND ";
        if (name != null) sqlString += "NAME = '" + name + "' AND ";
        if (prise >= 0) sqlString += "PRISE = " + prise + " AND ";
        if ((firstDate != null) && (secondDate!= null)) sqlString += "DATE BETWEEN '" + firstDate
                + "' AND '" + secondDate + "'";

        sqlString += " ORDER BY _id DESC";
        return sqlString;
    }

    public static String createMessageSQLString(){
        String sqlString = "SELECT * FROM " + DB_BUSINESS + " ORDER BY _id DESC";
        return sqlString;
    }

    public static void delete(Context context,String tableName, int id) {
        SQLiteOpenHelper openHelper = new DatabaseHelper(context);
        SQLiteDatabase db = openHelper.getReadableDatabase();
        db.execSQL("DELETE FROM " + tableName + " WHERE _id =" + id);
        db.close();
    }

    public static int getNotificationId(Context context) {
        int id = 0;
        SQLiteOpenHelper openHelper = new DatabaseHelper(context);
        SQLiteDatabase db = openHelper.getReadableDatabase();
        Cursor cursor = db.rawQuery("SELECT NOT_ID FROM " + DatabaseHelper.DB_BUSINESS ,
                null);
        if (cursor.moveToLast()) {
            id = cursor.getInt(0);
        }
        return id;
    }

    public static void deleteCotegory(Context context, int categoryId){
        SQLiteOpenHelper openHelper = new DatabaseHelper(context);
        SQLiteDatabase db = openHelper.getReadableDatabase();
        db.execSQL("DELETE FROM " + DB_COSTS + " WHERE CATEGORY_ID = " + categoryId);
        db.execSQL("DELETE FROM " + DB_COSTS_CATEGORY + " WHERE _id = " + categoryId);
        db.close();
    }
}
