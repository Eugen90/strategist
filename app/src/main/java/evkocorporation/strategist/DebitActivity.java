package evkocorporation.strategist;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Intent;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.SearchView;
import android.support.v7.widget.Toolbar;
import android.view.ContextMenu;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.widget.DatePicker;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Map;

import evkocorporation.strategist.Entities.Date;
import evkocorporation.strategist.Entities.Pay;

public class DebitActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener{
    private int firstDateId = 1;
    private int secondDateId = 2;
    private TextView sum;
    private Date date = new Date();
    private TextView firstText;
    private TextView secondText;
    private RecyclerView recyclerView;
    private CardAdapter adapter;
    private int itemPosition;
    private String firstDate;
    private String secondDate;
    private SearchView searchView;
    ArrayList<Pay> payList = new ArrayList<>();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_debit);
        //Запуск панели
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        //Кнопка бургер
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar,
                R.string.open_drawer,
                R.string.close_drawer);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        //Панель навигации
        NavigationView navigation =(NavigationView) findViewById(R.id.nav_view);
        navigation.setNavigationItemSelectedListener(this);
        //Текущая активность выделенна
        navigation.setCheckedItem(R.id.nav_debits);
        //Инициализация списка
        recyclerView = (RecyclerView) findViewById(R.id.recycler_view);
        //Инициализация поиска
        searchView = (SearchView) findViewById(R.id.search_view);
        searchPay();
        //Всегда на год меньше
        firstText = (TextView) findViewById(R.id.first_date);
        firstText.setText( date.receivingNumber(date.gettDay()) + "." + date.receivingNumber(date.gettMonth() + 1) + "."
                + date.receivingNumber(date.gettYear() - 1));
        //Текущая дата
        secondText = (TextView) findViewById(R.id.second_date);
        secondText.setText(date.receivingNumber(date.gettDay()) + "."
                + date.receivingNumber(date.gettMonth() + 1) + "."
                + date.receivingNumber(date.gettYear()));
        //Получает сумму
        sum = (TextView) findViewById(R.id.sum);
        sum.setText(sumPrise());
        //Заполнение списка
        loadDatabase();
        //Регистрация контекстного меню
        registerForContextMenu(recyclerView);
    }

    //Поиск
    private void searchPay(){
        searchView.setOnQueryTextListener(new SearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String word) {
                return false;
            }

            @Override
            public boolean onQueryTextChange(String word) {
                if (!word.equals("")) {
                    ArrayList<Pay> searchList = new ArrayList<>();
                    for (Pay pay : payList) {
                        if (pay.getTitle().contains(word)) {
                            searchList.add(pay);
                        }
                    }
                    CardAdapter searchAdapter = new CardAdapter(DebitActivity.this, searchList);
                    recyclerView.setAdapter(searchAdapter);
                }else{
                    recyclerView.setAdapter(adapter);
                }
                return false;
            }
        });
    }

    //Заполнение RecyclerView из БД
    public void loadDatabase() {
        String sqlString = DatabaseHelper.createSqlString(this, DatabaseHelper.DB_DEBIT,
                null, date.reReceivingDate(firstText.getText().toString()),
                date.reReceivingDate(secondText.getText().toString()),
                "1", null, null, -1);
        payList = DatabaseHelper.getPay(this, "debit", sqlString);

        LinearLayoutManager linearLayoutManager = new LinearLayoutManager(this);
        adapter = new CardAdapter(this, payList);
        recyclerView.setLayoutManager(linearLayoutManager);
        recyclerView.setHasFixedSize(true);
        recyclerView.setAdapter(adapter);
        //вывод контекстного меню
        adapter.setListener(new CardAdapter.Listener() {
            @Override
            public void onLongClick(View v ,int position) {
                itemPosition = position;
                v.showContextMenu();
            }
        });
    }

    //Реакция на щелчки по панели
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        int id = item.getItemId();
        Intent intent = null;

        switch (id) {
            case R.id.nav_main:
                intent = new Intent(this, MainActivity.class);
                break;
            case R.id.nav_coming:
                intent = new Intent(this, ComingActivity.class);
                break;
            case R.id.nav_costs:
                intent = new Intent(this, CostsActivity.class);
                break;
            case R.id.nav_i_debits:
                intent = new Intent(this, IDebitActivity.class);
                break;
            case R.id.nav_business:
                intent = new Intent(this, BusinessActivity.class);
                break;
            default:
                intent = new Intent(this, DebitActivity.class);
        }

        startActivity(intent);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    //закрытие панели при нажатии кнопки назад
    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            Intent intent = new Intent(this, MainActivity.class);
            startActivity(intent);
        }
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return super.onCreateOptionsMenu(menu);
    }

    //Открытие активности по щелчку на +
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.create_new:
                Intent intent = new Intent(this, CreateDebitActivity.class);
                startActivity(intent);
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    //Сумма
    private String sumPrise() {
        String sqlString = DatabaseHelper.createSqlString(this, DatabaseHelper.DB_DEBIT, "sum",
                date.reReceivingDate(firstText.getText().toString()),
                date.reReceivingDate(secondText.getText().toString()),
                "1", null, null, -1);
        String sum = "Сумма: " + DatabaseHelper.getSum(this, sqlString) + "р.";
        return sum;
    }

    //Клик на первой дате
    public void secondDateClick(View view) {
        showDialog(secondDateId);
    }
    //клик на второй дате
    public void firstDateClick(View view) {
        showDialog(firstDateId);
    }
    //Создание диалогового меню с календарем
    protected Dialog onCreateDialog(int id) {
        if (id == firstDateId) {
            Map<String, Integer> dayMonthYear = date.getDayMontYear(firstDate);
            DatePickerDialog tpd = new DatePickerDialog(
                    this, firstDateSet, dayMonthYear.get("year"), dayMonthYear.get("month")-1,
                    dayMonthYear.get("day"));
            return tpd;
        } else {
            DatePickerDialog tpd = new DatePickerDialog(
                    this, secondDateSet, date.gettYear(), date.gettMonth(), date.gettDay());
            return tpd;
        }
    }
    //Установка первой даты, которую выбрал пользователь
    DatePickerDialog.OnDateSetListener firstDateSet = new DatePickerDialog.OnDateSetListener() {

        public void onDateSet(DatePicker view, int year, int month,
                              int day) {
            date.settYear(year);
            date.settMonth(month);
            date.settDay(day);
            String newDate = date.receivingNumber(day) + "."
                    + date.receivingNumber(month + 1) + "."
                    + date.receivingNumber(year);
            firstDate = date.reReceivingDate(newDate);
            firstText.setText(newDate);
            loadDatabase();
            sum.setText(sumPrise());
        }
    };
    //Установка второй даты, которую выбрал пользователь
    DatePickerDialog.OnDateSetListener secondDateSet = new DatePickerDialog.OnDateSetListener() {

        public void onDateSet(DatePicker view, int year, int month,
                              int day) {
            date.settYear(year);
            date.settMonth(month);
            date.settDay(day);
            String newDate = date.receivingNumber(day) + "."
                    + date.receivingNumber(month + 1) + "."
                    + date.receivingNumber(year);
            secondText.setText(newDate);
            secondDate = date.reReceivingDate(newDate);
            loadDatabase();
            sum.setText(sumPrise());
        }
    };

    //Получение данных для сохдания контекстного меню
    @Override
    public void onCreateContextMenu(ContextMenu menu, View v, ContextMenu.ContextMenuInfo menuInfo) {
        super.onCreateContextMenu(menu, v, menuInfo);

        MenuInflater inflater = this.getMenuInflater();
        inflater.inflate(R.menu.context_menu, menu);
    }

    //Действия при нажатии на контекстное меню
    @Override
    public boolean onContextItemSelected(MenuItem item) {
        Pay pay = payList.get(itemPosition);
        int id = item.getItemId();
        //Кнопка удалить
        if (id == R.id.del) {
            DatabaseHelper.delete(this, DatabaseHelper.DB_DEBIT, pay.getId());
            payList.remove(itemPosition);
            recyclerView.getAdapter().notifyDataSetChanged();
            sum.setText(sumPrise());
            Toast.makeText(this, "Запись успешно удалена!", Toast.LENGTH_SHORT).show();
        }
        //кнопка изменить
        if (id == R.id.chan) {
            Bundle bundle = new Bundle();
            bundle.putString("title", pay.getTitle());
            bundle.putString("price", Integer.toString(pay.getPrice()));
            bundle.putString("id", Integer.toString(pay.getId()));
            Intent intent = new Intent(this, CreateDebitActivity.class);
            intent.putExtras(bundle);
            startActivity(intent);
        }
        return true;
    }
}
