package evkocorporation.strategist;

import android.content.Intent;
import android.content.SharedPreferences;
import android.support.v7.app.ActionBar;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.Spinner;
import android.widget.Toast;

import java.util.ArrayList;

import evkocorporation.strategist.Entities.Category;
import evkocorporation.strategist.Entities.Date;

public class DeleteCategoryActivity extends AppCompatActivity {
    private ArrayList<Category> categoryList = new ArrayList<>();
    private Spinner spinner;
    private int categoryId;
    private String textCategory;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_delete_category);
        //Запуск панели
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        //Кнопка назад
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        spinner = (Spinner) findViewById(R.id.spinner);

        SharedPreferences preferences = getSharedPreferences("properties", MODE_PRIVATE);
        textCategory = preferences.getString("category", "Все категории");

        follSpinner();
        spinnerSelect();
    }

    //Выбор категории
    private void spinnerSelect(){
        spinner.setOnItemSelectedListener(new AdapterView.OnItemSelectedListener() {
            @Override
            public void onItemSelected(AdapterView<?> parent, View view,
                                       int position, long id) {
                categoryId = categoryList.get(position).getId();
            }
            @Override
            public void onNothingSelected(AdapterView<?> arg0) {
            }
        });
    }

    //Заполнение списка
    private void follSpinner() {
        categoryList = DatabaseHelper.getCategory(this);
        ArrayAdapter<Category> adapter = new ArrayAdapter<>(this,
                android.R.layout.simple_spinner_item, categoryList);
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
        spinner.setAdapter(adapter);
        categoryId = getCategoryByName(textCategory);
        spinner.setSelection(categoryId);
    }

    //Создание записи по шелчку на галочке
    public void onClickDone(View view) {
        String messageText = "Категория успешно удалена!";
        DatabaseHelper.deleteCotegory(this, categoryId);
        //Сообщаем инфу пользователю
        Toast.makeText(this, messageText, Toast.LENGTH_SHORT).show();

        Intent intent = new Intent(this, CreateCostsActivity.class);
        startActivity(intent);
    }

    private int getCategoryByName(String name){
        for(int i = 0; i < categoryList.size(); i++){
            if (categoryList.get(i).getCategory().equals(name)) return i;
        }
        return 0;
    }
}
