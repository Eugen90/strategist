package evkocorporation.strategist.Entities;

public class Category {
    int id;
    String category;

    @Override public String toString() {
        return this.category;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }
}
