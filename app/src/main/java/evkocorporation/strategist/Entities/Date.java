package evkocorporation.strategist.Entities;

import java.text.SimpleDateFormat;
import java.util.HashMap;
import java.util.Map;

public class Date {
    private String date;
    private int tDay;
    private int tMonth;
    private int tYear;

    public String getDate() {
        date = new SimpleDateFormat("yyyy/MM/dd").format(new java.util.Date());
        return date;
    }

    public Map<String, Integer> getDayMontYear(String date){
        String tempDate = date.replace('.','/');
        String[] temp = tempDate.split("/");
        Map<String, Integer> dayMontYear =new HashMap<>();
        dayMontYear.put("day", Integer.parseInt(temp[0]));
        dayMontYear.put("month", Integer.parseInt(temp[1]));
        dayMontYear.put("year", Integer.parseInt(temp[2]));
        return dayMontYear;
    }

    public String reReceivingDate(String date) {
        String nDate = date.replace('.', '/');
        String[] today = nDate.split("/");
        String newDate = (today[2] + "/" + today[1] + "/" + today[0]);
        return newDate;
    }

    private String[] dayMonthYear(String date) {
        String[] today = date.split("/");
        return today;
    }

    public String receivingNumber(int number) {
        String strNumber = "";
        if (number < 10) {
            strNumber = "0" + Integer.toString(number);
        } else {
            strNumber = Integer.toString(number);
        }
        return strNumber;
    }

    public String receivingNumber(String number) {
        String strNumber = "";
        if (Integer.parseInt(number) < 10) {
            strNumber = "0" + number;
        } else {
            strNumber = number;
        }
        return strNumber;
    }

    public int gettYear() {
        String[] mDate = dayMonthYear(getDate());
        int tYear = Integer.parseInt(mDate[0]);
        return tYear;
    }

    public int gettDay() {
        String[] mDate = dayMonthYear(getDate());
        int tDay = Integer.parseInt(mDate[2]);
        return tDay;
    }

    public int gettMonth() {
        String[] mDate = dayMonthYear(getDate());
        int tMonth = Integer.parseInt(mDate[1])-1;
        return tMonth;
    }

    public void settDay(int tDay) {
        this.tDay = tDay;
    }

    public void settMonth(int tMonth) {
        this.tMonth = tMonth;
    }

    public void settYear(int tYear) {
        this.tYear = tYear;
    }
}