package evkocorporation.strategist.Entities;

//Платежи
public class Pay {
    private int id;
    private String title;
    private int price;
    private String datePay;
    private String category;

    public void setTitle(String title) {
        this.title = title;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public void setDatePay(String datePay) {
        this.datePay = datePay;
    }

    public String getTitle() {
        return title;
    }

    public int getPrice() {
        return price;
    }

    public String getDatePay() {
        return datePay;
    }

    public String getCategory() {
        return category;
    }

    public void setCategory(String category) {
        this.category = category;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
}





