package evkocorporation.strategist;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Intent;
import android.content.SharedPreferences;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.support.design.widget.NavigationView;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.DatePicker;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Map;

import evkocorporation.strategist.Entities.Date;

public class MainActivity extends AppCompatActivity implements NavigationView.OnNavigationItemSelectedListener {
    private SQLiteDatabase db;
    private Cursor cursor;
    private int firstDateId = 1;
    private int secondDateId = 2;
    private TextView comingSum;
    private TextView costsSum;
    private TextView myDebitsSum;
    private TextView meDebitsSum;
    private TextView balanceSum;
    private Date date = new Date();
    private TextView firstText;
    private TextView secondText;

    private SharedPreferences preferences;
    private String savedDate;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        //Запуск панели
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        //Кнопка бургер
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        ActionBarDrawerToggle toggle = new ActionBarDrawerToggle(this, drawer, toolbar,
                R.string.open_drawer,
                R.string.close_drawer);
        drawer.addDrawerListener(toggle);
        toggle.syncState();
        //Панель навигации
        NavigationView navigation =(NavigationView) findViewById(R.id.nav_view);
        navigation.setNavigationItemSelectedListener(this);
        //Текущая активность выделенна
        navigation.setCheckedItem(R.id.nav_main);
        //Читает сохрененное число из файла
        firstText = (TextView) findViewById(R.id.first_date);
        String actualDay = "01." + date.receivingNumber(date.gettMonth() + 1) + "."
                + date.receivingNumber(date.gettYear());
        preferences = getSharedPreferences("properties", MODE_PRIVATE);
        savedDate = preferences.getString("date", actualDay);
        firstText.setText(savedDate);
        //Текущая дата
        secondText = (TextView) findViewById(R.id.second_date);
        secondText.setText(date.receivingNumber(date.gettDay()) + "."
                + date.receivingNumber(date.gettMonth() + 1) + "."
                + date.receivingNumber(date.gettYear()));
        //инициализируем переменные
        comingSum = (TextView) findViewById(R.id.coming_sum);
        costsSum = (TextView) findViewById(R.id.costs_sum);
        myDebitsSum = (TextView) findViewById(R.id.my_debits);
        meDebitsSum = (TextView) findViewById(R.id.me_debits);
        balanceSum = (TextView) findViewById(R.id.balance);
        //Получает сумму
        makeFoll();
    }

    private void makeFoll() {
        int coming = sumPrise(DatabaseHelper.DB_COMING);
        int costs = sumPrise(DatabaseHelper.DB_COSTS);
        comingSum.setText(Integer.toString(coming) + "р.");
        costsSum.setText(Integer.toString(costs) + "р.");
        myDebitsSum.setText(Integer.toString(sumPrise(DatabaseHelper.DB_DEBIT, 0)) + "р.");
        meDebitsSum.setText(Integer.toString(sumPrise(DatabaseHelper.DB_DEBIT, 1)) + "р.");
        balanceSum.setText(Integer.toString(coming - costs) + "р.");
    }

    public void secondDateClick(View view) {
        showDialog(secondDateId);
    }

    public void firstDateClick(View view) {
        showDialog(firstDateId);
    }

    //Реакция на щелчки по панели
    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        int id = item.getItemId();
        Intent intent = null;

        switch (id) {
            case R.id.nav_coming:
                intent = new Intent(this, ComingActivity.class);
                break;
            case R.id.nav_costs:
                intent = new Intent(this, CostsActivity.class);
                break;
            case R.id.nav_debits:
                intent = new Intent(this, DebitActivity.class);
                break;
            case R.id.nav_i_debits:
                intent = new Intent(this, IDebitActivity.class);
                break;
            case R.id.nav_business:
                intent = new Intent(this, BusinessActivity.class);
                break;
            default:
                intent = new Intent(this, MainActivity.class);
        }

        startActivity(intent);

        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        drawer.closeDrawer(GravityCompat.START);
        return true;
    }

    //закрытие панели при нажатии кнопки назад
    @Override
    public void onBackPressed() {
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else {
            super.onBackPressed();
        }
    }
    //Сумма
    private int sumPrise(String dBTable) {
        int sum = 0;
        SQLiteOpenHelper dbHelper = new DatabaseHelper(this);
        try {
            db = dbHelper.getReadableDatabase();
                cursor = db.rawQuery("SELECT SUM(PRISE) FROM " + dBTable
                        + " WHERE DATE BETWEEN '"
                        + date.reReceivingDate(firstText.getText().toString())
                        + "' AND '" + date.reReceivingDate(secondText.getText().toString())
                        + "'",null);
            if (cursor.moveToFirst()) {
                sum = cursor.getInt(0);
            }
        } catch (Exception e) {
            Toast toast = Toast.makeText(this, "Ошибка базы данных", Toast.LENGTH_SHORT);
            toast.show();
        }
        return sum;
    }

    //Сумма
    private int sumPrise(String dBTable, int variable) {
        int sum = 0;
        SQLiteOpenHelper dbHelper = new DatabaseHelper(this);
        try {
            db = dbHelper.getReadableDatabase();
            cursor = db.rawQuery("SELECT SUM(PRISE) FROM " + dBTable
                    + " WHERE VARIABLE = " + variable ,null);
            if (cursor.moveToFirst()) {
                sum = cursor.getInt(0);
            }
        } catch (Exception e) {
            Toast toast = Toast.makeText(this, "Ошибка базы данных", Toast.LENGTH_SHORT);
            toast.show();
        }
        return sum;
    }
    //Создание диалогового меню с календарем
    protected Dialog onCreateDialog(int id) {
        if (id == firstDateId) {
            Map<String, Integer> dayMonthYear = date.getDayMontYear(savedDate);
            DatePickerDialog tpd = new DatePickerDialog(
                    this, firstDateSet, dayMonthYear.get("year"), dayMonthYear.get("month")-1,
                    dayMonthYear.get("day"));
            return tpd;
        } else {
            DatePickerDialog tpd = new DatePickerDialog(
                    this, secondDateSet, date.gettYear(), date.gettMonth(), date.gettDay());
            return tpd;
        }
    }
    //Установка первой даты, которую выбрал пользователь
    DatePickerDialog.OnDateSetListener firstDateSet = new DatePickerDialog.OnDateSetListener() {

        public void onDateSet(DatePicker view, int year, int month,
                              int day) {
            date.settYear(year);
            date.settMonth(month);
            date.settDay(day);
            String newDate = date.receivingNumber(day) + "."
                    + date.receivingNumber(month + 1) + "."
                    + date.receivingNumber(year);
            firstText.setText(newDate);
            //Сохранение в файл
            preferences = getSharedPreferences("properties", MODE_PRIVATE);
            SharedPreferences.Editor editor = preferences.edit();
            editor.putString("date", newDate);
            editor.commit();
            makeFoll();
        }
    };
    //Установка второй даты, которую выбрал пользователь
    DatePickerDialog.OnDateSetListener secondDateSet = new DatePickerDialog.OnDateSetListener() {

        public void onDateSet(DatePicker view, int year, int month,
                              int day) {
            date.settYear(year);
            date.settMonth(month);
            date.settDay(day);
            String newDate = date.receivingNumber(day) + "."
                    + date.receivingNumber(month + 1) + "."
                    + date.receivingNumber(year);
            secondText.setText(newDate);
            makeFoll();
        }
    };

    public void comingClick(View view) {
        Intent intent = new Intent(this, ComingActivity.class);
        startActivity(intent);
    }

    public void meDebitsClick(View view) {
        Intent intent = new Intent(this, DebitActivity.class);
        startActivity(intent);
    }

    public void debitsClick(View view) {
        Intent intent = new Intent(this, IDebitActivity.class);
        startActivity(intent);
    }

    public void costsClick(View view) {
        Intent intent = new Intent(this, CostsActivity.class);
        startActivity(intent);
    }
}
